class NoMatchesException(Exception):
    """
    Raised if grammar rule doesn't match.
    """


class ParsingException(Exception):
    """
    Raised if input cannot be parsed.
    """
