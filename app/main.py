import argparse
import csv

from exceptions import ParsingException
from typing import List

from eval import (
    create_rpn,
    filter_file,
)
from grammar import (
    build_syntax_tree,
    get_rules,
)


def argument_parser() -> argparse.ArgumentParser:
    """
    Put this in function to simplify testing.
    """
    parser = argparse.ArgumentParser(description="Filter table with expressions")
    parser.add_argument("--file", default="data/example.csv")
    args = parser.parse_args()
    return args


def print_rows(rows: List[dict]):
    header = ",".join(rows[0].keys())
    body = [",".join(row.values()) for row in rows]
    table = "\n".join([header] + body)
    print(table)


def main() -> int:
    # set useful vars
    finished = False
    rules = get_rules()
    verbose_tree = True

    args = argument_parser()
    with open(args.file, "r") as file:
        file_content = list(csv.DictReader(file))
    # print short instruction
    print(
        """Please insert an expression. 

Type:
-> `show` for printing dataset
-> `quiet` for less verbose tree output or `verbose`
-> `q` to quit\n"""
    )
    while not finished:
        text = input(">")

        # switch cases based on input
        if text == "show":
            print_rows(file_content)
            continue
        elif text == "quiet":
            verbose_tree = False
            continue
        elif text == "verbose":
            verbose_tree = False
            continue
        elif text == "q":
            return 0
        else:
            # expression was inserted, perform analysis
            try:
                syntax_tree = build_syntax_tree(text, rules)
            except ParsingException as exc:
                print(f"Syntax error: {exc.args[0]}\n Please try again.")
                continue
            else:
                syntax_tree.print_tree(verbose=verbose_tree)
                print(
                    f"Do you want to filter file {args.file} with above syntax tree? [y]"
                )
                confirmation = input(">")

                if confirmation == "y":
                    # eval and display results
                    reordered_tokens = create_rpn(syntax_tree.get_token_list())
                    filtered_rows = filter_file(args.file, reordered_tokens)
                    if filtered_rows:
                        print_rows(filtered_rows)
                    else:
                        print("No matched rows in dataset")
                else:
                    print("Expression won't be evaluated")


if __name__ == "__main__":
    main()
