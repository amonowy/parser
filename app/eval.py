import csv

from exceptions import ParsingException
from typing import (
    Callable,
    List,
)

# assign operators to precedence
operators = {"or": 1, "and": 2}


def create_rpn(tokens: List[str]) -> List[str]:
    """
    Converts list of tokens to Reverse Polish notation, which is later used to
    evaluate expressions.

    Dijkstra shunting-yard algorithm is used.
    """
    operator_stack = []
    output = []
    while tokens:
        token = tokens.pop(0)
        if token not in operators.keys() and token not in ("(", ")"):
            # node is expression like a=1
            output.append(token)
        elif token == "(":
            operator_stack.append(token)
        elif token == ")":
            try:
                while operator_stack[-1] != "(":
                    output.append(operator_stack.pop(-1))
            except IndexError:
                raise ParsingException("Additional right parenthesis in expression")
            if operator_stack[-1] == "(":
                operator_stack.pop(-1)
        else:
            # is logical operator
            try:
                while (operators[operator_stack[-1]] > operators[token]) and (
                    operator_stack[-1] != "("
                ):
                    output.append(operator_stack.pop(-1))
            except IndexError:
                """
                Operator stack is empty, proceed.
                """
            except KeyError:
                """
                Current token is left or right parenthesis, proceed.
                """
            operator_stack.append(token)

    while operator_stack:
        # if any operators left
        if operator_stack[-1] == "(":
            raise ParsingException("Additional left parenthesis in expression")
        output.append(operator_stack.pop(-1))
    return output


def filter_file(filename: str, tokens: List[str]) -> List[dict]:
    with open(filename, "r") as file:
        reader = csv.DictReader(file)
        rows = list(reader)

    result = []
    for row in rows:
        int_row = {col: int(val) for col, val in row.items()}
        if eval_rpn(tokens, evaluate_expr, int_row):
            result.append(row)
    return result


def eval_rpn(
    parsed_tokens: List[str], evaluate_function: Callable, row: dict = None
) -> bool:
    """
    Evaluate input using reversed polish notation.
    """
    # copy list, because we need to mutate it
    tokens = list(parsed_tokens)
    stack = []
    if len(tokens) == 1:
        return evaluate_function(tokens.pop(), row=row)
    for token in tokens:
        if token in operators.keys():
            operand_2 = stack.pop(-1)
            operand_1 = stack.pop(-1)
            result = evaluate_function(operand_2, operand_1, token, row)
            stack.append(result)
        else:
            stack.append(token)
    result = stack.pop()

    return result


def evaluate_expr(
    operand_2: str, operand_1: str = None, operator: str = None, row: dict = None
) -> bool:
    """
    Function used to evaluate expressions.
    """
    if operator and operand_1:
        args = [operand_1, operator, operand_2]
    else:
        args = [operand_2]

    args = try_replace_equality(args)

    str_to_eval = " ".join(str(arg) for arg in args)
    return eval(str_to_eval, row)


def try_replace_equality(args: List[str]) -> List[str]:
    """
    Helper function to prepare equality check in Python syntax.
    """
    new_args = []
    for arg in args:
        try:
            if arg[0].isalpha() and arg[1] == "=":
                arg = arg.replace("=", "==")
            new_args.append(arg)
        except TypeError:
            new_args.append(arg)

    return new_args
