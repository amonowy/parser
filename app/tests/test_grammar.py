from unittest.mock import patch

import pytest

from grammar import (
    GRAMMAR_FILE,
    build_syntax_tree,
    get_rules,
    parse_rules,
    prepare_components,
    read_grammar,
    remove_whitespaces,
)


def parse_input_helper(text: str):
    rules = get_rules()
    syntax_tree = build_syntax_tree(text, rules)
    return syntax_tree


@pytest.mark.parametrize("example_str", [" foo bar ", "new line \n"])
def test_remove_whitespaces(example_str):
    new_str = remove_whitespaces(example_str)
    assert " " not in new_str
    assert "\n" not in new_str


def test_parser_rules():
    rules = read_grammar(GRAMMAR_FILE)
    parsed = parse_rules(rules)
    for name, rules in parsed.items():
        assert type(rules) is list
        assert len(rules) > 0


class TestParser:
    @pytest.fixture
    def grammar(self):
        return read_grammar(GRAMMAR_FILE)

    @pytest.fixture(autouse=True)
    def mock_grammar(self, grammar):
        with patch("grammar.read_grammar", return_value=grammar):
            yield

    @pytest.mark.parametrize(
        "grammar",
        [
            """integer = <digit> <optional_integer>
optional_integer = <integer> | ;
digit = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9| 0""".splitlines()
        ],
    )
    def test_single_integer_parser(self, grammar):
        with patch("grammar.is_valid_query", return_value=True):
            result = parse_input_helper("9")
            assert len(result.children) == 2
            assert [i.value for i in result.children] == ["9", ";"]

    @pytest.mark.parametrize(
        "grammar",
        [
            """integer = <digit> <optional_integer>
optional_integer = <integer> | ;
digit = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9| 0""".splitlines()
        ],
    )
    def test_compound_integer_parser(self, grammar):
        with patch("grammar.is_valid_query", return_value=True):
            result = parse_input_helper("123")
            assert result.children[0].value == "1"

    def test_expression_parser(self, grammar):
        tree = parse_input_helper("d>=987")
        values = [node.cumulated_value for node in tree.children]
        assert "d>=987" in values

    @pytest.mark.parametrize(
        "text",
        [
            "((f=4 or b>9) and d=5)",
            "(((f=4 or b>9) and d=5) or a=4)",
            "((((f=4 or b>9) and d=5) or a=4) and a=1)",
            "(f=4 and b>9) or (d>=45 or h<4)",
            "(f=4 and b>9 or d>=45)",
            "f=4 and (b>9 or d>=45)",
            "f=4 and b>9 or d>=45",
            "d>4 and c<9",
            "d=4 and c>9",
            "d=-1",
            "d=-1 or c > -9",
            "(f=4 and b>-9) or (d>=-45 or h<4)",
            "a>b",
            "(a=1)",
            "(a=1) and (b=1)",
        ],
    )
    def test_parenthesized_logical(self, text):
        tree = parse_input_helper(text)
        assert tree.cumulated_value == text.replace(" ", "")

    def test_get_token_list(self):
        tree = parse_input_helper("(f=834 and b>9) or (d>=45 or h<4)")
        tokens = tree.get_token_list()


@pytest.mark.parametrize(
    "components, expected", [(">=", ">="), ("<=", "<="), ("<", "<"), ("<", "<")]
)
def test_prepare_components(components, expected):
    prepared = prepare_components(components)
    assert prepared == [expected]
