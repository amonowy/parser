from itertools import chain
from unittest.mock import (
    Mock,
    patch,
)

import main
import pytest

from eval import (
    create_rpn,
    filter_file,
)
from grammar import (
    build_syntax_tree,
    get_rules,
)


@pytest.mark.parametrize(
    "input_text, expected",
    [
        ["a>=0", 4],
        ["a>2", 1],
        ["a>=1", 3],
        ["a=1", 1],
        ["a=1 and a=6", 0],
        ["a=1 or a=0", 2],
        ["a=0 or a=1 and a=0", 1],
    ],
)
def test_filter_file(input_text, expected):
    file = "data/example.csv"
    rules = get_rules()
    syntax_tree = build_syntax_tree(input_text, rules)
    reordered_tokens = create_rpn(syntax_tree.get_token_list())
    result = filter_file(file, reordered_tokens)
    assert len(result) == expected


@pytest.mark.parametrize(
    "user_input",
    [
        "a=1",
        "a<3 or b>4 and c<2",
        "a>=3 or b>=4 and c<=2",
        "(a>=3 or b>=4) and c<=2",
        "a=2 and b<4 or c<1723",
        "a=2 and b<4 and c>1723",
        "(((((a=1)))))"
    ],
)
def test_main_py(user_input, capsys):
    """
    Quick sanity test.
    """
    with patch("main.input", side_effect=chain([user_input, "y", "q"])), patch(
        "main.argument_parser", return_value=Mock(file="data/example.csv")
    ):
        main.main()
        out = capsys.readouterr().out
        printed_cols = "a,b,c,d,e,f,g,h"
        assert printed_cols in out or "No matched rows in dataset"


@pytest.mark.parametrize(
    "user_input", ["p", "123", "=", " ", "\n", "\t", "afsadfsadfa"]
)
def test_invalid_input(user_input, capsys):
    with patch("main.input", side_effect=chain([user_input, "q"])), patch(
        "main.argument_parser", return_value=Mock(file="data/example.csv")
    ):
        main.main()
        out = capsys.readouterr().out
        assert "Syntax error" in out
