from exceptions import ParsingException

import pytest

from eval import (
    create_rpn,
    eval_rpn,
    filter_file,
)


def evaluate_bool(operand_2, operand_1, operator, row=None):
    """
    Helper function for tests.
    """
    str_to_eval = " ".join(str(item) for item in [operand_1, operator, operand_2])
    return eval(str_to_eval)


@pytest.mark.parametrize(
    "tokens, expected",
    [
        [["d", "or", "f"], ["d", "f", "or"]],
        [["d", "or", "f", "and", "a"], ["d", "f", "a", "and", "or"]],
        [["(", "d", "or", "f", ")", "and", "a"], ["d", "f", "or", "a", "and"]],
        [
            ["(", "d", "or", "f", "and", "b", ")", "and", "a"],
            ["d", "f", "b", "and", "or", "a", "and"],
        ],
        [
            ["(", "(", "d", "or", "f", ")", "and", "a", ")"],
            ["d", "f", "or", "a", "and"],
        ],
        [
            ["(", "(", "(", "d", "or", "f", ")", "and", "a", ")", "or", "x", ")"],
            ["d", "f", "or", "a", "and", "x", "or"],
        ],
    ],
)
def test_simple_create_rpn(tokens, expected):
    output = create_rpn(tokens)
    assert output == expected


@pytest.mark.parametrize(
    "tokens",
    [
        ["d", "or", "f", ")", "and", "a"],
        ["(", "d", "or", "f", "and", "a"],
        ["(", "(", "d", "or", "f", ")", "and", "a", ")", "or", "x", ")"],
    ],
)
def test_create_rpn_raises_if_unmatched_parens(tokens):
    with pytest.raises(ParsingException) as exc:
        output = create_rpn(tokens)
    assert exc


@pytest.mark.parametrize(
    "rpn_expr, expected",
    [
        [["True", "True", "and"], True],
        [["True", "False", "or"], True],
        [["True", "False", "or", "False", "and"], False],
        [["True", "False", "True", "or", "and"], True],
        [["True", "False", "True", "and", "or"], True],
        [["True", "False", "True", "and", "and"], False],
    ],
)
def test_rpn_evaluation(rpn_expr, expected):
    result = eval_rpn(rpn_expr, evaluate_bool)
    assert result is expected


@pytest.mark.parametrize(
    "expression, expected",
    [
        [["a>=0"], 4],
        [["a>2"], 1],
        [["a>=1"], 3],
        [["a=1"], 1],
        [["a=1", "a=6", "and"], 0],
        [["a=1", "a=0", "or"], 2],
        [["b>a"], 4],
        [["b<a"], 0],
        [["b>a", "b<a", "and"], 0],
        [["b>a", "b<a", "or"], 4],
    ],
)
def test_filter_file(expression, expected):
    file = "data/example.csv"
    result = filter_file(file, expression)
    assert len(result) == expected
