import string

from collections import OrderedDict
from exceptions import (
    NoMatchesException,
    ParsingException,
)
from typing import (
    Dict,
    List,
    Tuple,
)

GRAMMAR_FILE = "grammar.txt"
VALID_QUERIES = ["expression", "query", "paren_logical", "logical"]


def read_grammar(filename: str) -> list:
    with open(filename, "r") as file:
        return file.readlines()


def remove_whitespaces(text: str) -> str:
    """
    Remove whitespace chars without regex.
    """
    whitespace_map = {char: "" for char in string.whitespace}
    translations = str.maketrans(whitespace_map)
    return text.translate(translations)


def parse_rules(rules: List[str]) -> Dict[str, list]:
    """
    Prepare rules for easier parsing.
    """
    # order of rules matter so use OrderedDict
    parser_rules = OrderedDict()
    for rule in rules:
        if rule.startswith("#"):
            # its comment, continue
            continue
        name, definition = rule.split("=", 1)
        clean_definition = remove_whitespaces(definition)
        alternatives = clean_definition.split("|")
        parser_rules[name.strip()] = alternatives
    return parser_rules


def get_rules(grammar_file: str = GRAMMAR_FILE) -> Dict[str, list]:
    grammar = read_grammar(grammar_file)
    return parse_rules(grammar)


def build_syntax_tree(text: str, rules: Dict[str, list]):
    """
    Function for handling user input.

    Iterates over every rules and checks is entire input matches. Return root
    node of parsing tree.
    """
    trimmed_text = text.replace(" ", "")
    for rule, alternatives in rules.items():
        try:
            node, left_text = check_rule(trimmed_text, rules, rule, first_level=True)
            if left_text:
                continue
            else:
                break
        except NoMatchesException:
            pass

    if left_text:
        raise ParsingException(f"Invalid query - {text}")
    elif not is_valid_query(node):
        raise ParsingException(f"Text {text} cannot be evaluated")
    return node


def is_valid_query(node):
    return node.rule in VALID_QUERIES


def prepare_components(alternative: str) -> List[str]:
    """
    Split alternative into components, if non terminal rule.
    """
    if "<" in alternative and ">" in alternative:
        components = alternative.split("<")
        components.remove("")  # empty str is result of split
        return components
    else:
        return [alternative]


class Node:
    def __init__(self, value: str = ""):
        self.children = []
        self.value = value
        self.alternative = ""
        self.rule = ""

    @property
    def _value(self) -> str:
        if self.value:
            return self.value + f" (<{self.rule}>)"
        return f"nonterminal (<{self.rule}>) --> " + self.alternative

    @property
    def cumulated_value(self):
        """
        Returns concatenated vaues of children nodes.

        This method is useful for evaluating order of expressions when
        parentheses and logical operators are used.
        """
        value = self.value if self.value != ";" else ""
        for child in self.children:
            value += child.cumulated_value
        return value

    def print_tree(
        self,
        child: "Node" = None,
        prefix: str = "",
        last: bool = True,
        verbose: bool = True,
    ) -> None:
        if child == None:
            child = self

        if verbose:
            node_str = child._value
        else:
            node_str = child.value or "()"

        if not child.is_hidden:
            # don't print end of match (";") node
            print(prefix, "`- " if last else "|- ", node_str, sep="")

        prefix += "   " if last else "|  "
        child_count = len(child.children)
        for i, child in enumerate(child.children):
            last = i == (child_count - 1)
            self.print_tree(child, prefix, last, verbose)

    def get_token_list(self) -> List["Node"]:
        if self.is_hidden:
            return []
        if not self.children or self.rule == "expression":
            return [self.cumulated_value]
        else:
            tokens = []
            for child in self.children:
                tokens.extend(child.get_token_list())
            return tokens

    @property
    def is_hidden(self):
        """
        `end of match` nodes are hidden in tree.
        """
        return self.value == ";"

    def __str__(self) -> str:
        return self.cumulated_value

    def __repr__(self) -> str:
        return f"Node(val={self.value or 'proxy'}, {self.rule})"


def check_rule(
    text: str, rules: Dict[str, list], rule: str, first_level=False
) -> Tuple[Node, str]:
    """
    Main function responsible for building parsing tree.

    Its recursive descend LL parser with support for defining
    alternatives in grammar file (with | as alternative separator).

    Term component means part of text which should be separately
    handled eg grammar: expression=<digit><bool><digit> has 3 components:
    digit, bool and digit.

    scope_text variable contains text which was not parsed yet .
    """
    alternatives = rules[rule]
    for alternative in alternatives:
        components = prepare_components(alternative)
        node = Node()
        node.alternative = alternative
        node.rule = rule
        scope_text = text
        for component in components:
            if component.endswith(">") and len(component) > 1:
                # component is nonterminal
                # omit last letter from component name - its ">" nonterminal identifier
                try:
                    new_node, left_text = check_rule(
                        scope_text, rules, component[:-1]
                    )
                except NoMatchesException:
                    # component doesn't match
                    break

                node.children.append(new_node)
                scope_text = left_text
            else:
                # component is terminal
                if scope_text.startswith(component):
                    node.value = component
                    # cut scope text because it matched
                    scope_text = scope_text[len(component) :]
                    return node, scope_text
                elif component == ";":
                    # return end of match symbol
                    node.value = ";"
                    return node, scope_text
                else:
                    break
        else:
            if len(node.children) != len(components):
                continue
            elif first_level and left_text:
                continue
            else:
                return node, left_text

    if node.children:
        if len(node.children) != len(components):
            raise NoMatchesException("Alternative doesn't match")
        return node, scope_text
    raise NoMatchesException("Given rule doesnt match")
