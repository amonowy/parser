# Quick start
* use python 3.6 or newer
* install requirements from `requirements.txt` - `pip install -r requirements.txt`
* enter `app/` cataloge
* run interpreter with `python main.py`
* run `python main.py --help` for help

You can specify csv for filtering with `--file` flag.

In interpreter you can switch between `quiet` and `verbose` 
which changes tree verbosity.

# Parser

Parser uses Dijkstra shunting-yard algorithm for boolean 
expressions parsing, with recursive descent grammar parser.

The idea is to use shunting-yard algorithm to prepare RPN 
for evaluation, then evaluate expression.

## Grammar

Grammar is defined in `grammar.txt` file. Grammar syntax 
allows to specify alternatives with `|` operator.

Nonterminal identifiers are specified inside `<>` like 
`<nonterminal>` in rules definitions.

## Tests

Pytest is requred for tests. To run test suite enter 
`app/` and run tests with `pytest`

### TODO:
* Add safer evaluation of expressions with `ast` module instead of `eval`

